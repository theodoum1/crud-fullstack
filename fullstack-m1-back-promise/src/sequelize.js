const { Sequelize } = require('sequelize');

const sequelize = new Sequelize('crud_fullstack', 'root', '', {
  host: 'localhost',
  dialect: 'mysql'
});

var exports = module.exports = {};
exports.sequelize = sequelize;

const Characters = sequelize.define('characters', {
    id: {type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true},
    firstName: {type: Sequelize.STRING(250), allowNull: false},
    lastName: {type: Sequelize.STRING(250), allowNull: false},
    birthYear: {type: Sequelize.STRING(250), allowNull: false},
},
    {tableName: 'characters', timestamp: false, underscored: true}
);

var exports = module.exports = {};
exports.Characters = Characters;