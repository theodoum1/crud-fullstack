import { PrimaryKeyError } from '../common/error/repository-error.model';
import { ICharacter } from './character.model';

const sequelize = require('../sequelize.js');

const getStorePromise = (): Promise<ICharacter[]> => {
  return sequelize.Characters.findAll().then(res => { 
    const tab_res: ICharacter[] = res.map(item => {
      return {
        id: item.getDataValue('id'),
        firstName: item.getDataValue('firstName'),
        lastName: item.getDataValue('lastName'),
        birthYear: item.getDataValue('birthYear'),
      }
    });
    return Promise.resolve(tab_res);
  });
};

const createCharacterPromise = (item): Promise<ICharacter> => {
  return sequelize.Characters.create(item).then(res => {
    console.log(res);
  });
}

export const findAll = (): Promise<ICharacter[]> => {
  return getStorePromise();
};

export const get = (id: number): Promise<ICharacter> => {
  console.log("Test");
  return getStorePromise().then(models => {
    console.log(models);
    if (models[id]) {
      console.log(models[id]);
      return models[id];
    }
    throw new PrimaryKeyError();
  });
};

export const create = (model: ICharacter): Promise<ICharacter> => {
  return createCharacterPromise(model);
};

export const update = (id: number, model: ICharacter): Promise<ICharacter> => {
  return getStorePromise()
    .then(models => {
      if (models[id]) {
        models.splice(id, 1, model);
        return model;
      }
      throw new PrimaryKeyError();
    });
};

export const remove = (id: number): Promise<ICharacter> => {
  return getStorePromise()
    .then(models => {
      if (models[id]) {
        const [model] = models.splice(id, 1);
        return model;
      }
      throw new PrimaryKeyError();
    });
};