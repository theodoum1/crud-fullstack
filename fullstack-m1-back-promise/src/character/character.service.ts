import { itemErrorHandler } from '../common/error/error.mapper';
import { dtoToModel, modelToDto } from './character.mapper';
import { ICharacterDto } from './character.model';
import {
  create as repositoryCreate,
  findAll as repositoryFindAll,
  get as repositoryGet,
  remove as repositoryRemove,
  update as repositoryUpdate
} from './character.repository';

export const findAll = (): Promise<ICharacterDto[]> => {
  return repositoryFindAll()
    .then(characters => characters.map(modelToDto));
};

export const get = (id: number): Promise<ICharacterDto> => {
  return repositoryGet(id)
    .then(modelToDto)
    .catch(itemErrorHandler(id));
};

export const create = (dto: ICharacterDto): Promise<ICharacterDto> => {
  const character = dtoToModel(dto);
  return repositoryCreate(character)
    .then(modelToDto);
};

export const update = (id: number, dto: ICharacterDto): Promise<ICharacterDto> => {
  const character = dtoToModel(dto);
  return repositoryUpdate(id, character)
    .then(modelToDto)
    .catch(itemErrorHandler(id));
};

export const remove = (id: number): Promise<ICharacterDto> => {
  return repositoryRemove(id)
    .then(modelToDto)
    .catch(itemErrorHandler(id));
};
