export interface ICharacter {
  //id: string,
  firstName: string;
  lastName: string;
  birthYear: number;
}

export interface ICharacterDto {
  //id: string,
  firstName: string;
  lastName: string;
  age: number;
}
