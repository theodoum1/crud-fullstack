import moment from 'moment';
import { ICharacter, ICharacterDto } from './character.model';

export const modelToDto = (model: ICharacter): ICharacterDto => {
  return {
    //id: model.id,
    firstName: model.firstName,
    lastName: model.lastName,
    age: moment().year() - model.birthYear
  };
};

export const dtoToModel = (dto: ICharacterDto): ICharacter => {
  return {
    //id: dto.id,
    firstName: dto.firstName,
    lastName: dto.lastName,
    birthYear: moment().subtract(dto.age, 'year').year()
  };
};
