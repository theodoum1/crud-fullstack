import moment from 'moment';
import { IMovie, IMovieDto } from './movie.model';

export const modelToDto = (model: IMovie): IMovieDto => {
  return {
    title: model.title,
    date: moment(model.date).format()
  };
};

export const dtoToModel = (dto: IMovieDto): IMovie => {
  return {
    title: dto.title,
    date: moment(dto.date).valueOf()
  };
};
