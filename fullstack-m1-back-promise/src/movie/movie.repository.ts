import { PrimaryKeyError } from '../common/error/repository-error.model';
import store from './movie.json';
import { IMovie } from './movie.model';

const getStorePromise = (): Promise<IMovie[]> => Promise.resolve(store);

export const findAll = (): Promise<IMovie[]> => {
  return getStorePromise();
};

export const get = (id: number): Promise<IMovie> => {
  return getStorePromise().then(models => {
    if (models[id]) {
      return models[id];
    }
    throw new PrimaryKeyError();
  });
};

export const create = (model: IMovie): Promise<IMovie> => {
  return getStorePromise()
    .then(models => {
      models.push(model);
      return model;
    });
};

export const update = (id: number, model: IMovie): Promise<IMovie> => {
  return getStorePromise()
    .then(models => {
      if (models[id]) {
        models.splice(id, 1, model);
        return model;
      }
      throw new PrimaryKeyError();
    });
};

export const remove = (id: number): Promise<IMovie> => {
  return getStorePromise()
    .then(models => {
      if (models[id]) {
        const [model] = models.splice(id, 1);
        return model;
      }
      throw new PrimaryKeyError();
    });
};
