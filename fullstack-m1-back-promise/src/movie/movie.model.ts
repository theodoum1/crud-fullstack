export interface IMovie {
  title: string;
  date: number;
}

export interface IMovieDto {
  title: string;
  date: string;
}
