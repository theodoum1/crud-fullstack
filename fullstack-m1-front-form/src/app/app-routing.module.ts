import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CharacterEditComponent } from './character/character-edit/character-edit.component';
import { CharacterGetComponent } from './character/character-get/character-get.component';
import { CharacterComponent } from './character/character.component';
import { MovieComponent } from './movie/movie.component';

const routes: Routes = [
  {
    path: 'characters',
    component: CharacterComponent
  },
  {
    path: 'characters/edit',
    component: CharacterEditComponent
  },
  {
    path: 'characters/:id',
    component: CharacterGetComponent
  },
  {
    path: 'movies',
    component: MovieComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
