export interface ICharacterDto {
  firstName: string;
  lastName: string;
  age: number;
}
