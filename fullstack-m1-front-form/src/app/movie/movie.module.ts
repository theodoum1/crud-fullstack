import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MovieComponent } from './movie.component';
import { MovieListComponent } from './movie-list/movie-list.component';
import { MovieItemComponent } from './movie-item/movie-item.component';

@NgModule({
  declarations: [
    MovieComponent,
    MovieListComponent,
    MovieItemComponent
  ],
  imports: [
    CommonModule,
    MatCardModule
  ],
  exports: [
    MovieComponent
  ]
})
export class MovieModule { }
