import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CharacterItemAttribute, ICharacter } from '../character.model';
import { CharacterService } from '../character.service';

@Component({
  selector: 'app-character-get',
  templateUrl: './character-get.component.html',
  styleUrls: ['./character-get.component.scss'],
  providers: [
    CharacterService
  ]
})
export class CharacterGetComponent {

  id = this.route.snapshot.paramMap.get('id');
  item: ICharacter;
  constructor(private route: ActivatedRoute, private service: CharacterService) { 
    this.getCharacter(this.id);
  }

  getCharacter(id: any){
      this.service.getItem(id).subscribe(data => this.item=data);
  }
}
