import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterGetComponent } from './character-get.component';

describe('CharacterGetComponent', () => {
  let component: CharacterGetComponent;
  let fixture: ComponentFixture<CharacterGetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CharacterGetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterGetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
