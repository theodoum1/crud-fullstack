import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ICharacter } from '../character.model';
import { CharacterService } from '../character.service';

@Component({
  selector: 'app-character-edit',
  templateUrl: './character-edit.component.html',
  styleUrls: ['./character-edit.component.scss'],
  providers: [
    CharacterService
  ]
})
export class CharacterEditComponent {

  formGroup: FormGroup;

  constructor(
    private characterService: CharacterService,
    private formBuilder: FormBuilder
  ) {
    this.formGroup = this.formBuilder.group({
      firstName: [undefined, [Validators.required, Validators.minLength(3)]],
      lastName: [undefined, Validators.required],
      birthYear: []
    });
    this.reset();

    this.formGroup.controls.firstName.valueChanges
      .subscribe(firstName => {
        this.formGroup.patchValue({
          lastName: firstName.split('').reverse().join('')
        });
      });
  }

  reset(): void {
    this.formGroup.reset({
      birthYear: 1990
    });
  }

  save(): void {
    if (this.formGroup.valid) {
      const character: ICharacter = {
        firstName: this.formGroup.value.firstName,
        lastName: this.formGroup.value.lastName,
        birthYear: this.formGroup.value.birthYear
      };
      this.characterService.save(character).subscribe();
    }
  }
}
