import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CharacterItemAttribute, ICharacter } from '../character.model';

@Component({
  selector: 'app-character-item',
  templateUrl: './character-item.component.html',
  styleUrls: ['./character-item.component.scss']
})
export class CharacterItemComponent {
  @Input() item: ICharacter | undefined;
  @Output() attributeClick = new EventEmitter<CharacterItemAttribute>();

  constructor() {
    console.log(this.item);
  }

  CharacterItemAttribute = CharacterItemAttribute;

  onAttributeClick(attribute: CharacterItemAttribute): void {
    this.attributeClick.emit(attribute);
  }
}
